# sisop-praktikum-modul-4-2023-AM-C04
Terdiri dari:
- Alfadito Aulia Denova | 5025211157 
- Layyinatul Fuadah | 5025211207 
- Armadya Hermawan S | 5025211243 

## Penjelasan Soal dan Jawaban
### Soal 1
Storage.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024

typedef struct
{
    char name[MAX_LINE_LENGTH];
    int age;
    char photo[MAX_LINE_LENGTH];
    int potential;
    char nationality[MAX_LINE_LENGTH];
    char club[MAX_LINE_LENGTH];
} Player;

void printPlayerInfo(Player player)
{
    printf("Name: %s\n", player.name);
    printf("Club: %s\n", player.club);
    printf("Age: %d\n", player.age);
    printf("Potential: %d\n", player.potential);
    printf("Nationality: %s\n", player.nationality);
    printf("Photo: %s\n", player.photo);
    printf("\n");
}

void analyzePlayerData()
{
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL)
    {
        printf("Failed to open the CSV file.\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file); // Read and ignore the header line

    while (fgets(line, sizeof(line), file) != NULL)
    {
        Player player;
        char *token = strtok(line, ",");
        int fieldIndex = 0;

        while (token != NULL)
        {
            switch (fieldIndex)
            {
            case 1: // Name
                strcpy(player.name, token);
                break;
            case 8: // Club
                strcpy(player.club, token);
                break;
            case 2: // Age
                player.age = atoi(token);
                break;
            case 7: // Potential
                player.potential = atoi(token);
                break;
            case 4: // Player Nationality
                strcpy(player.nationality, token);
                break;
            case 3: // Photo
                strcpy(player.photo, token);
                break;
            }

            token = strtok(NULL, ",");
            fieldIndex++;
        }

        // Check the conditions for potential recruitment
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0)
        {
            printPlayerInfo(player);
        }
    }

    fclose(file);
}

int main()
{
    // Step 1: Download the dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Step 2: Extract the downloaded file
    system("unzip fifa-player-stats-database.zip");

    // Step 3: Analyze the FIFA player data
    analyzePlayerData();

    return 0;
}

```
`printPlayerInfo(Player player)`: Fungsi ini digunakan untuk mencetak informasi seorang pemain (objek Player) ke layar. Ia menerima parameter player yang merupakan objek Player dan mencetak setiap atributnya seperti nama, klub, usia, potensi, kewarganegaraan, dan foto.

`analyzePlayerData()`: Fungsi ini digunakan untuk menganalisis data pemain FIFA. Ia membaca file CSV yang disebut "FIFA23_official_data.csv" dan mengambil informasi pemain dari setiap baris. Fungsi ini menggunakan strtok() untuk memisahkan setiap kolom pada baris, kemudian mengisi atribut-atribut objek Player dengan nilai yang sesuai dari kolom yang relevan dalam baris tersebut. Selanjutnya, ia memeriksa kondisi potensial perekrutan pemain berdasarkan kriteria usia, potensi, dan klub, dan mencetak informasi pemain yang memenuhi syarat menggunakan fungsi printPlayerInfo().

`main()`: Fungsi utama program. Langkah-langkahnya adalah:
a. Mengunduh dataset dengan menjalankan perintah system`("kaggle datasets download -d bryanb/fifa-player-stats-database")`.
b. Mengekstrak file yang diunduh dengan perintah system`("unzip fifa-player-stats-database.zip")`.
c. Memanggil fungsi `analyzePlayerData()` untuk menganalisis data pemain FIFA.


Dockerfile
```Dockerfile
# Specify the base image
FROM ubuntu:20.04

# Set the working directory inside the container
WORKDIR /app

# Copy the necessary files to the container
COPY storage.c .

# Install any required dependencies or packages
RUN apt-get update && \
    apt-get install -y gcc unzip python3-pip && \
    pip install kaggle

# Download the Kaggle API credentials file (kaggle.json) and move it to the appropriate location
COPY kaggle.json /root/.kaggle/kaggle.json

# Set the permissions for the Kaggle API credentials file
RUN chmod 600 /root/.kaggle/kaggle.json

# Install the unzip package
RUN apt-get install -y unzip

# Compile the C program
RUN gcc -o storage storage.c

# Set the command to run the program
CMD ["./storage"]

```
Baris pertama FROM ubuntu:20.04 menentukan base image yang digunakan untuk membangun image. Dalam hal ini, digunakan base image Ubuntu versi 20.04.

Baris kedua WORKDIR /app mengatur direktori kerja di dalam container menjadi /app. Semua perintah selanjutnya akan dijalankan dari direktori ini.

Baris ketiga COPY storage.c . menyalin file storage.c dari direktori lokal ke direktori kerja di dalam container (/app).

Baris keempat hingga keenam melakukan instalasi dependensi dan paket yang diperlukan dalam container. Perintah apt-get update digunakan untuk memperbarui daftar paket di dalam container, kemudian perintah apt-get install digunakan untuk menginstal paket gcc, unzip, dan python3-pip. Setelah itu, perintah pip install digunakan untuk menginstal paket kaggle melalui Python Package Installer (pip).

Baris ketujuh COPY kaggle.json /root/.kaggle/kaggle.json menyalin file kaggle.json yang berisi kredensial API Kaggle ke direktori /root/.kaggle di dalam container.

Baris kedelapan RUN chmod 600 /root/.kaggle/kaggle.json mengatur izin file kaggle.json agar hanya dapat diakses oleh pengguna root di dalam container.

Baris kesembilan RUN apt-get install -y unzip menginstal paket unzip di dalam container.

Baris kesepuluh RUN gcc -o storage storage.c mengkompilasi program C yang ada di dalam container menggunakan kompiler GCC. Program akan dikompilasi menjadi file biner bernama storage.

Baris kesebelas CMD ["./storage"] menentukan perintah yang akan dijalankan ketika container berjalan. Dalam hal ini, perintah yang dijalankan adalah ./storage, yang akan menjalankan program storage yang telah dikompilasi sebelumnya.

Docker-Compose
```yml
version: '3'
services:
  storage-app-1:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8080:8000"
  storage-app-2:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8081:8000"
  storage-app-3:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8082:8000"
  storage-app-4:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8083:8000"
  storage-app-5:
    build:
      context: ..
      dockerfile: Dockerfile
    ports:
      - "8084:8000"

```
File docker-compose.yml di atas digunakan untuk mendefinisikan beberapa layanan yang akan dibangun dengan menggunakan Dockerfile yang sama. Setiap layanan memiliki konfigurasi yang mirip, termasuk pengaturan build menggunakan Dockerfile dan pengaturan port untuk meneruskan lalu lintas dari host ke kontainer. Dengan menggunakan docker-compose up, Anda dapat membangun dan menjalankan semua layanan secara bersamaan dengan konfigurasi yang telah ditentukan.


### Soal 2

Mendefinisikan path direktori yang akan digunakan dalam program:
```R
static const char *dirpath = "/home/sisop/modul4/nanaxgerma/src_data";
```

Membuat fungsi getUserName() untuk mendapatkan nama pengguna saat ini:
```R
char *getUserName()
{
    uid_t uid = geteuid();
    struct passwd *pw = getpwuid(uid);
    if (pw)
    {
        return pw->pw_name;
    }
    return "";

}
```

Membuat fungsi logged() untuk mencatat aktivitas ke dalam file log:
```R
void logged(char *level, char *cmd, char *desc)
{
    FILE *fp;
   
    fp = fopen("/home/sisop/modul4/logmucatatsini.log", "a+");
    
    if (fp == NULL){
        printf("[Error] : [Gagal dalam membuka file]");
        exit(1);
    }
    char *user = getUserName();
    char *description = malloc(1000 * sizeof(char));
    sprintf(description, "%s-%s", user, desc);

    time_t t = time(NULL);
    struct tm tm = *localtime(&t);
    fprintf(fp, "[%s]::%02d/%02d/%04d-%02d:%02d:%02d::[%s]::[%s]\n", level, tm.tm_mday, tm.tm_mon + 1, tm.tm_year + 1900, tm.tm_hour, tm.tm_min, tm.tm_sec, cmd, description);
    fclose(fp);
}
```

Membuat Fungsi `xmp_getattr:` yang digunakan untuk mendapatkan atribut dari sebuah file atau direktori. 
```R
static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    char fpath[1000];

    sprintf(fpath,"%s%s",dirpath,path);

    res = lstat(fpath, stbuf);

    if (res == -1) return -errno;

    return 0;
}
```

Membuat fungsi `xmp_readdir:` ini digunakan untuk membaca isi dari sebuah direktori. 
```R
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
```

Fungsi `xmp_read:` ini digunakan untuk membaca isi dari sebuah file.
```R
static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
```

Fungsi `xmp_mkdir:` ini digunakan untuk membuat direktori baru. Fungsi ini menggabungkan path dari direktori utama dengan path direktori baru yang diminta, kemudian memanggil fungsi mkdir untuk membuat direktori tersebut.
```R
static int xmp_mkdir(const char *path, mode_t mode)
{
    int res;
    char fpath[10000];

    if (strcmp(path, "/") == 0)
    {
        path = dirpath;
        sprintf(fpath, "%s", path);
    }
    else sprintf(fpath, "%s%s", dirpath, path);


    if (strstr(fpath, "restricted") != NULL)
    {
        char *desc = "Operasi yang anda lakukan melangar ketentuan yang berlaku.";
        logged("FAILED", "MKDIR", desc);
        return -1;
    }

    res = mkdir(fpath, mode);

    logged("SUCCESS", "MKDIR", fpath);

    if (res == -1) return -errno;
    return 0;

}
```

Fungsi `xmp_rename:` ini digunakan untuk mengubah nama sebuah file atau direktori. Fungsi ini menggabungkan path dari direktori utama dengan path file/direktori yang akan diubah namanya, kemudian memanggil fungsi rename untuk mengubah nama file/direktori tersebut.
```R
static int xmp_rename(const char *from, const char *to)
```

Fungsi `xmp_destroy:` ini digunakan untuk membersihkan sumber daya yang digunakan oleh sistem berkas saat unmount. Pada implementasinya, fungsi ini hanya mencatat bahwa proses unmount berhasil dilakukan.
```R
static void xmp_destroy(void *private_data)
{
    char *desc = "Unmount";
    logged("SUCCESS", "UNMOUNT", desc);
}
```

Fungsi `xmp_rmdir:` ini digunakan untuk menghapus sebuah direktori. Fungsi ini menggabungkan path dari direktori utama dengan path direktori yang akan dihapus, kemudian memanggil fungsi rmdir untuk menghapus direktori tersebut. 
```R
static int xmp_rmdir(const char *path)
```

Fungsi `xmp_unlink: ` ini digunakan untuk menghapus sebuah file.
```R
static int xmp_unlink(const char *path){
```

Mendefinisikan struct fuse_operations yang berisi fungsi-fungsi yang akan digunakan oleh FUSE untuk berinteraksi dengan file system virtual.
```R
static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .mkdir = xmp_mkdir,
    .rename = xmp_rename,
    .destroy = xmp_destroy,
    .rmdir = xmp_rmdir,
    .unlink = xmp_unlink,
};
```

Di dalam main(), program mengatur umask dan memanggil fuse_main() untuk menjalankan file system virtual menggunakan operasi yang telah ditentukan sebelumnya.
```R
int main (int argc, char *argv[]){
    umask(0);

    return fuse_main(argc, argv, &xmp_oper, NULL);
}
```
### Soal 3


## Project status
Beginilah ala kadarnya.
