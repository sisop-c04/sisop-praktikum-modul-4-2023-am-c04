#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 1024

typedef struct
{
    char name[MAX_LINE_LENGTH];
    int age;
    char photo[MAX_LINE_LENGTH];
    int potential;
    char nationality[MAX_LINE_LENGTH];
    char club[MAX_LINE_LENGTH];
} Player;

void printPlayerInfo(Player player)
{
    printf("Name: %s\n", player.name);
    printf("Club: %s\n", player.club);
    printf("Age: %d\n", player.age);
    printf("Potential: %d\n", player.potential);
    printf("Nationality: %s\n", player.nationality);
    printf("Photo: %s\n", player.photo);
    printf("\n");
}

void analyzePlayerData()
{
    FILE *file = fopen("FIFA23_official_data.csv", "r");
    if (file == NULL)
    {
        printf("Failed to open the CSV file.\n");
        return;
    }

    char line[MAX_LINE_LENGTH];
    fgets(line, sizeof(line), file); // Read and ignore the header line

    while (fgets(line, sizeof(line), file) != NULL)
    {
        Player player;
        char *token = strtok(line, ",");
        int fieldIndex = 0;

        while (token != NULL)
        {
            switch (fieldIndex)
            {
            case 1: // Name
                strcpy(player.name, token);
                break;
            case 8: // Club
                strcpy(player.club, token);
                break;
            case 2: // Age
                player.age = atoi(token);
                break;
            case 7: // Potential
                player.potential = atoi(token);
                break;
            case 4: // Player Nationality
                strcpy(player.nationality, token);
                break;
            case 3: // Photo
                strcpy(player.photo, token);
                break;
            }

            token = strtok(NULL, ",");
            fieldIndex++;
        }

        // Check the conditions for potential recruitment
        if (player.age < 25 && player.potential > 85 && strcmp(player.club, "Manchester City") != 0)
        {
            printPlayerInfo(player);
        }
    }

    fclose(file);
}

int main()
{
    // Step 1: Download the dataset
    system("kaggle datasets download -d bryanb/fifa-player-stats-database");

    // Step 2: Extract the downloaded file
    system("unzip fifa-player-stats-database.zip");

    // Step 3: Analyze the FIFA player data
    analyzePlayerData();

    return 0;
}
