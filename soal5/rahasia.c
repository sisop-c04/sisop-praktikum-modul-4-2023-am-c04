// rahasia.c

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/time.h>


static  int  xmp_getattr(const char *path, struct stat *stbuf)
{
    int res;
    res = lstat(path, stbuf);

    if (res == -1) return -errno;
    return 0;
}



static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void) offset;
    (void) fi;

    dp = opendir(path);

    if (dp == NULL) return -errno;

    while ((de = readdir(dp)) != NULL) {
        struct stat st;

        memset(&st, 0, sizeof(st));

        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;

        if(filler(buf, de->d_name, &st, 0)) break;
    }
    closedir(dp);
    return 0;
}



static int xmp_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res;
    (void) fi;

    fd = open(path, O_RDONLY);

    if (fd == -1) return -errno;

    res = pread(fd, buf, size, offset);

    if (res == -1) res = -errno;

    close(fd);

    return res;
}



static struct fuse_operations xmp_oper = {
    .getattr = xmp_getattr,
    .readdir = xmp_readdir,
    .read = xmp_read,
    .rename = xmp_rename,
};



void download_dan_unzip(){
	//somehow hanya bisa mendownload 2,2 kb ??? 
	char* command_dl = "wget -O rahasia.zip https://drive.google.com/uc?id=18YCFdG658SALaboVJUHQIqeamcfNY39a&export=download" ;
	system(command_dl);
	
	//sleep(5);
	char* command = "unzip -q rahasia.zip -d rahasia" ;
	system(command);
	
}

int main(){
	char* comm = "ls | grep rahasia.zip";
	int status_folder = system(comm);
	printf("%d \n", status_folder);
	
	if (status_folder)
	{	download_dan_unzip();
	
	}
	else
	{ printf("file sudah ada\n");
	}
	
	//melakukan mount dari folder rahasia
	umask(0);
	
	
	
	return 0;
}
